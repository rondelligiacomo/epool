

<?php


if(isset($operazione)){

	try{
		// Connessione a MongoDB
		$manager = new MongoDB\Driver\Manager("mongodb://localhost:27017");

		// Inizializzo var Bulk
		$bulk = new MongoDB\Driver\BulkWrite;

		// Inserimento
		$bulk->insert(['log ' => $operazione]);

		// Esegue il comando sul db Log, che se non esiste viene creato
		$manager->executeBulkWrite('epool.mongo', $bulk);
	} catch(MongoDB\Driver\Exception\Exception $e) {
		echo "<br><br><br>".$e -> getMessage(), "\n";
		exit;
	}
}
?>

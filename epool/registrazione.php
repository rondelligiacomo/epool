<?php
include("db_con.php");
?>



<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script>
  $(document).ready(function(){
    $('#comboAzienda').hide();
  });


  $(function() {
    $('#tipoutente').change(function(){
      if ( $('#tipoutente').val() == 'A' ){
        $('#comboAzienda').show();
      }else {
        $('#comboAzienda').hide();
      }
    });
  });
  </script>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<h2>Registrazione</h2>
<form name="form_registration" method="post" action="registrazione_do.php">
<br/>
<p>Indirizzo Email: <input type="text" name="username_reg"></p>
<br/>
<p>Password: <input type="password" name="password_reg"></p>
<br/>
<p>Nome: <input type="text" name="nome_reg" ></p>
<br/>
<p>Cognome: <input type="text" name="cognome_reg"></p>
<br/>
<p>Data di Nascita (aaaa-mm-gg) : <input type="text" name="datanascita_reg" ></p>
<br/>
<p>Luogo di Nascita: <input type="text" name="luogonascita_reg"></p>
<br/>
<p>Tipo Utente:
<select id="tipoutente" name="tipoutente">
                <option value = "S">Semplice</option>
                <option value = "A">Dipendente Azienda</option>
</select>
</p>

<div id="comboAzienda">
<p >Azienda:
  <select id="azienda" name="azienda">
    <?
    $sql="select codazienda, nome, indirizzo from azienda;";
    try {
      $stmt = $conn->prepare($sql);
      $stmt->execute();
    } catch (PDOException $e) {
        echo $e;
        exit();
    }
    $result = $stmt->fetchAll();


      foreach ($result as $row) {
    ?>
        <option value="<?=$row["codazienda"]?>"><?=$row["nome"]." - ".$row["indirizzo"]?></option>
    <?
      }
    ?>
  </select>
</p>
</div>
<br/>

<button>Registrati</button>
</form>
<body>
</html>

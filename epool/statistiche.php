<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>


  <?php
    include("funzioni.php");
    $operazione="visualizza pagina statistiche utente: ".$_SESSION['username'];
    require("mongo.php");
  ?>
  <br>
  <br>
  <br>
  <br>

  <h2>UTENTI MAGGIORMENTE ATTIVI</h2>
  <?php
  $sql= "select utente.indirizzoemail, utente.nome, cognome, count(idsegnalazione) as numero from utente left join segnalazione on utente.indirizzoemail=segnalazione.indirizzoemail group by utente.indirizzoemail, utente.nome, cognome; order by 4 desc ";

  try {
    $stmt = $conn->prepare($sql);
    $stmt->execute();
  } catch (PDOException $e) {
      echo $e;
      exit();
  }
  $result = $stmt->fetchAll();

  print "<table border='1' width='80%'>\n";
  print"<tr><th>Username</th><th>Nome</th><th>Cognome</th><th>Numero segnalazioni</th>";

  foreach ($result as $row) {
     print "<tr><td>".$row["indirizzoemail"]."</td><td>".$row["nome"]."</td><td>".$row["cognome"]."</td><td>".$row["numero"]."</td></tr>";
  }

  print "</table>\n";
  ?>

<br>
<br>


<h2>VOTI DEGLI UTENTI</h2>
<?php
$sql= "select utente.indirizzoemail, utente.nome, cognome, avg(voto) as voto from utente left join feedback on utente.indirizzoemail=feedback.indirizzoemail where utente.tipoutente in ('A', 'P') group by utente.indirizzoemail, utente.nome, cognome; order by 4 desc ";

try {
  $stmt = $conn->prepare($sql);
  $stmt->execute();
} catch (PDOException $e) {
    echo $e;
    exit();
}
$result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<tr><th>Username</th><th>Nome</th><th>Cognome</th><th>Voto medio</th>";
// print_r($result);
foreach ($result as $row) {
   print "<tr><td>".$row["indirizzoemail"]."</td><td>".$row["nome"]."</td><td>".$row["cognome"]."</td><td>".$row["voto"]."</td></tr>";
}

print "</table>\n";
?>


<br>
<br>



<h2>VEICOLI MAGGIROMENTE PRENOTATI</h2>
<?php
$sql= "select marca, modello, count(codiceprenotazione) as numero from veicolo left join prenotazione on veicolo.targa=prenotazione.targa group by marca, modello order by 3 desc;";

try {
  $stmt = $conn->prepare($sql);
  $stmt->execute();
} catch (PDOException $e) {
    echo $e;
    exit();
}
$result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<tr><th>Marca</th><th>Modello</th><th>Numero prenotazioni</th>";
// print_r($result);
foreach ($result as $row) {
   print "<tr><td>".$row["marca"]."</td><td>".$row["modello"]."</td><td>".$row["numero"]."</td></tr>";
}

print "</table>\n";
?>




  <body>
  </html>

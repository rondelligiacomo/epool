
<?php
function codifica_utente($tipo){
  if ($tipo=='A'){
    return "Dipendente Azienda";
  }
  if ($tipo=='P'){
    return "Utente Premium";
  }
  if ($tipo=='S'){
    return "Utente Semplice";
  }

}

function visualizza_data($data){
  if (is_null($data))
    return "";
  return substr($data, 8,10)."/".substr($data, 5,2)."/".substr($data,0,4);
}


include("db_con.php");
session_start();
$is_logged = $_SESSION['id_logged'];
if($is_logged){
?>
<span style="background-color: #000000">   <a href="accesso.php" title="Home" >DASHBOARD</a>
  |
 <a href="visualizza_profilo.php" title="Visualizza Profili" >PROFILO UTENTE</a>
  |
   <a href="statistiche.php" title="STATISTICHE" >STATISTICHE</a>
  |
   <a href="cerca_veicolo.php" title="Cerca Veicolo" >PRENOTA VEICOLO</a>
  |
  <?php
  if($_SESSION['tipo_utente']=='A' || $_SESSION['tipo_utente']=='P'){
  ?>
  <a href="visualizza_storico_premium.php" title="Visualizza Storico Prenotazione" >STORICO PRENOTAZIONI</a>
  |
  <?php
  }
  ?>
  
  <?php
  if($_SESSION['tipo_utente']=='S'){
  ?>
  <a href="visualizza_storico.php" title="Visualizza Storico Prenotazione" >STORICO PRENOTAZIONI</a>
  |
  <?php
  }
  ?>

   <a href="visualizza_societa.php" title="Visualizza Società Car Sharing" >SOCIETA' CAR SHARING</a>
  |
  <?php
  if($_SESSION['tipo_utente']=='A' || $_SESSION['tipo_utente']=='P'){
  ?>
   <a href="visualizza_carpooling.php" title="Visualizza Car Pooling" >CAR POOLING</a>
  |
  <?php
  }
  ?>

  <?php
  if($_SESSION['tipo_utente']=='P'){
  ?>
  <a href="votazione.php" title="votazione" >VOTA</a>
  |
  <?php
   }
   ?>
  <a href="logout.php" title="Esci" >LOG OUT</a>


      &nbsp &nbsp &nbsp &nbsp    &nbsp &nbsp &nbsp &nbsp     &nbsp &nbsp &nbsp &nbsp       &nbsp &nbsp &nbsp &nbsp
      &nbsp &nbsp &nbsp &nbsp    &nbsp &nbsp &nbsp &nbsp     &nbsp &nbsp &nbsp &nbsp       &nbsp &nbsp &nbsp &nbsp
      &nbsp &nbsp &nbsp &nbsp    &nbsp &nbsp &nbsp &nbsp     &nbsp &nbsp


     </span>

<?php
}else{
  header('Location: error.php');
}
?>

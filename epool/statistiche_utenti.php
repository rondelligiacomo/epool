<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>
<?php
  include("funzioni.php");
?>
<h2>Classifica degli utenti sulla base del voto medio</h2>
<?php
$sql= "select utente.indirizzoemail, utente.nome, cognome, avg(voto) as voto from utente left join feedback on utente.indirizzoemail=feedback.indirizzoemail where utente.tipoutente in ('A', 'P') group by utente.indirizzoemail, utente.nome, cognome; order by 4 desc ";

try {
  $stmt = $conn->prepare($sql);
  $stmt->execute();
} catch (PDOException $e) {
    echo $e;
    exit();
}
$result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<tr><th>Username</th><th>Nome</th><th>Cognome</th><th>Voto medio</th>";

foreach ($result as $row) {
   print "<tr><td>".$row["indirizzoemail"]."</td><td>".$row["nome"]."</td><td>".$row["cognome"]."</td><td>".$row["voto"]."</td></tr>";
}

print "</table>\n";
?>


<body>
</html>

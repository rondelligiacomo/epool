<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>
<?php
  include("funzioni.php");
?>
<h2>LE TUE PRENOTAZIONI </h2>

<?php
// STORICO DELLE PRENOTAZIONI
$sql = "select prenotazione.codiceprenotazione, veicolo.targa, veicolo.modello, veicolo.marca, veicolo.descrizione, veicolo.capienza, prenotazione.datainizio, prenotazione.orainizio, prenotazione.datafine, prenotazione.orafine, prenotazione.note, area_sosta.indirizzo as indirizzo_arrivo, area_sosta2.indirizzo as indirizzo_partenza ".
      " from prenotazione left join veicolo on prenotazione.targa = veicolo.targa left join area_sosta on area_sosta.latitudine = prenotazione.latitudineArrivo and area_sosta.longitudine = prenotazione.longitudineArrivo left join area_sosta area_sosta2 on prenotazione.latitudinePartenza = area_sosta2.latitudine and prenotazione.longitudinePartenza = area_sosta2.longitudine where indirizzoemail='".$_SESSION["username"]."'  order by veicolo.targa ";

        try {
          $stmt = $conn->prepare($sql);
          $stmt->execute();
        } catch (PDOException $e) {
            echo $e;
            exit();
        }
        $result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<th>Codice Prenotazione</th><th>Targa</th><th>Modello</th><th>Marca</th><th>Capienza</th><th>Descrizione</th><th>DataInizio</th><th>OraInizio</th><th>DataFine</th><th>OraFine</th><th>Note</th><th>Indirizzo Partenza</th><th>Indirizzo Arrivo</th>\n";


foreach ($result as $row) {
  print"<tr>";
   print "<td>".$row["codiceprenotazione"]."</td><td>".$row["targa"]."</td><td>".$row["modello"]."</td><td>".$row["marca"]."</td><td>".$row["capienza"]."</td><td>".$row["descrizione"]."</td><td>".visualizza_data($row["datainizio"])."</td><td>".$row["orainizio"]."</td><td>".visualizza_data($row["datafine"])."</td><td>".$row["orafine"]."</td><td>".$row["note"]."</td><td>".$row["indirizzo_partenza"]."</td><td>".$row["indirizzo_arrivo"]."</td>";
    print"</tr>";
}

print "</table>\n";


print "<h2>I TUOI CARPOOLING </h2>";
// STORICO DEI CAR POOLING
$sql = "select orario, ordine, citta, via
from condividi_tappa inner join tappa_tragitto on condividi_tappa.idtragitto = tappa_tragitto.idtragitto
inner join tappa on tappa.longitudine=tappa_tragitto.longitudine and tappa.latitudine=tappa_tragitto.latitudine
inner join tragitto on tragitto.idtragitto = tappa_tragitto.idtragitto
and condividi_tappa.indirizzoemail='".$_SESSION["username"]."' order by ordine" ;

        try {
          $stmt = $conn->prepare($sql);
          $stmt->execute();
        } catch (PDOException $e) {
            echo $e;
            exit();
        }
        $result = $stmt->fetchAll();

print "<table border='1' width='80%'>\n";
print"<th>Orario</th><th>Ordine</th><th>Citta</th><th>Via</th>\n";

foreach ($result as $row) {
  print"<tr>";
   print "<td>".$row["orario"]."</td><td>".$row["ordine"]."</td><td>".$row["citta"]."</td><td>".$row["via"]."</td>";
    print"</tr>";
}

print "</table>\n";
?>

<body>
</html>

<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>
  <?php
    include("funzioni.php");
  ?>
  <h2>INSERISCI TAPPE PER CAR POOLING</h2>

<?php
$op=$_GET['op'];
$id=0;


if($op==2){
    $id=$_POST['id'];
    $insert = "CALL INSERT_TAPPA(:idtragitto, :orario, :ordine, :citta, :via, :latitudine, :longitudine)";

  try {

    if($_POST['latitudine'] != '' && $_POST['longitudine'] !='')
    {
      $stmt=$conn->prepare($insert);
      $stmt->bindParam(":idtragitto", $id);
      $stmt->bindValue(":orario", $_POST['orario']);
      $stmt->bindValue(":ordine", $_POST['ordine']);
      $stmt->bindValue(":citta", $_POST['citta']);
      $stmt->bindValue(":via", $_POST['via']);
      $stmt->bindValue(":latitudine", $_POST['latitudine']);
      $stmt->bindValue(":longitudine", $_POST['longitudine']);
      $stmt->execute();


    }

    } catch (PDOException $e) {
      exit();
    }
}



if($op==1){
    $insert = "CALL INSERT_TRAGITTO(:tipologia, :num_km, :codiceprenotazione)";

  try {


    if($_POST['tipologia'] != '' && $_POST['num_km'] !='')
    {
      $stmt=$conn->prepare($insert);
      $stmt->bindParam(":tipologia", $_POST['tipologia']);
      $stmt->bindValue(":num_km", $_POST['num_km']);
      $stmt->bindValue(":codiceprenotazione", $_POST['codicep']);
      $stmt->execute();

      $stmt = $conn->query("SELECT LAST_INSERT_ID()");
      $id = $stmt->fetchColumn();


    }

    } catch (PDOException $e) {
      exit();
    }

}else{
  $id=$_REQUEST['id'];
}

  ?>
    <h3>... inserisci adesso le tappe!</h3>

    <form name="form_tragitto" method="post" action="abilita_car_pooling_do.php?op=2">
    <br/>
    <p>Ordine: <input type="text" name="ordine"></p>
    <p>Orario: <input type="text" name="orario"></p>

    <p>Citta': <input type="text" name="citta"></p>
    <p>Via: <input type="text" name="via"></p>
    <p>Latitudine: <input type="text" name="latitudine"></p>
    <p>Longitudine: <input type="text" name="longitudine"></p>

    <p><input type="hidden" name="id" value="<?=$id?>"></p>
    <button>Inserisci</button>
    </form>

    <br/><br/>

    <h2>Lista delle tappe del viaggio...</h2>
    <?php
    $sql= "select ordine, orario, citta, via, tappa.latitudine, tappa.longitudine from tappa_tragitto inner join tappa on tappa_tragitto.latitudine=tappa.latitudine and tappa_tragitto.longitudine=tappa.longitudine and tappa_tragitto.idtragitto=".$id." order by ordine;";


    try {
      $stmt = $conn->prepare($sql);
      $stmt->execute();
    } catch (PDOException $e) {
        echo $e;
        exit();
    }
    $result = $stmt->fetchAll();

    print "<table border='1' width='80%'>\n";
    print"<tr><th>Ordine</th><th>Orario</th><th>Citta'</th><th>Via</th></tr>";
    foreach ($result as $row) {
       print "<tr><td>".$row["ordine"]."</td><td>".$row["orario"]."</td><td>".$row["citta"]."</td><td>".$row["via"]."</td></tr>";
    }

    print "</table>\n";


?>

<html>
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Epool</title>
</head>
<body>
<?php
  include("funzioni.php");
  $targa="";
  if (isset($_GET["targa"])){
    $targa=$_GET["targa"];
  }

  $op="";
  if (isset($_GET["op"])){
    $op=$_GET["op"];
  }
  if (strcmp($op,"OK")!=0){
      ?>
      <h2>Insersici una segnalazione relativa ad un veicolo</h2>

      <form name="inserisci_segnalazione" method="post" action="inserisci_segnalazione.php?op=OK">
      <p>Data Odierna o Futura (aaaa-mm-gg)<br><input type="text" name="data" size="10"></p>
      <p>Titolo <br><input type="text" name="titolo" size="50"></p>
      <p>Testo <br><textarea name="testo" rows="6" cols="50"></textarea></p>
      <p>Targa <br><input type="text" readonly="readonly" name="targa" value="<?=$targa?>" size="10"></p>

      <input type="reset" name="Cancella" value="Cancella">
      <input type="submit" name="Inserisci" value="Inserisci Segnalazione">

      </form>
<?php
}else{


      $insert = "CALL INSERT_SEGNALAZIONE(:IndirizzoEmail, :Data, :Titolo, :Testo, :Targa)";


      if($_POST['titolo'] != '' && $_POST['testo'] !='' && $_POST['data'] !='' && $_POST['targa'] != '')
      {
          $stmt=$conn->prepare($insert);
          $stmt->bindParam(":IndirizzoEmail", $_SESSION["username"]);
          $stmt->bindValue(":Data", $_POST['data']);
          $stmt->bindValue(":Titolo", $_POST['titolo']);
          $stmt->bindValue(":Testo", $_POST['testo']);
          $stmt->bindValue(":Targa", $_POST['targa']);
        try {
          $stmt->execute();
        } catch (PDOException $e) {
            echo $e;
            exit();
        }
        $operazione="utente ".$_SESSION['username']." inserisce segnalazione in data: ".$_POST['data']." per il veicolo: ".$_POST['targa'];
        require("mongo.php");
      ?>
         <h3>La tua segnalazione e' stata registrata con successo!</h3>
    <?php
      }



}
?>

<body>
</html>

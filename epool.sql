drop database epool;

create database epool;

use epool;

-- CREO TABELLE

CREATE TABLE UTENTE(
IndirizzoEmail VARCHAR(50) NOT NULL PRIMARY KEY,
Pwd VARCHAR(32) NOT NULL,
Nome VARCHAR(50) NOT NULL,
Cognome VARCHAR(50) NOT NULL,
DataNascita DATE,
LuogoNascita VARCHAR(50),
CODazienda INT,
TipoUtente CHAR(1) 
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;


CREATE TABLE AZIENDA(
CODazienda INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
Nome VARCHAR(50) NOT NULL,
Indirizzo VARCHAR(255),
Telefono VARCHAR (20),
TelefonoResponsabile VARCHAR (20)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;


CREATE TABLE LAVORA(
IndirizzoEmail VARCHAR(50),
CODazienda INT,
PRIMARY KEY(IndirizzoEmail, CODazienda),
FOREIGN KEY (IndirizzoEmail) REFERENCES UTENTE(IndirizzoEmail),
FOREIGN KEY (CODazienda) REFERENCES AZIENDA(CODazienda)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE FOTO(
IDFoto INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
Nome VARCHAR(255),
IndirizzoEmail VARCHAR(50),
FOREIGN KEY (IndirizzoEmail) REFERENCES UTENTE(IndirizzoEmail)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE FEEDBACK(
IDFeedback INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
DataFeedback DATE,
Testo VARCHAR(500),
Voto INT, -- check da 0 a 10
IndirizzoEmail VARCHAR(50),
FOREIGN KEY (IndirizzoEmail) REFERENCES UTENTE(IndirizzoEmail)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE SOCIETA(
IDSocieta INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
NumeroVeicolo INT,
Nome VARCHAR(50),
URL VARCHAR(255),
NumeroTelefono VARCHAR(20),
Tipo CHAR(2) -- PU pubblica e PR privata
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE AREA_SOSTA(
Longitudine VARCHAR(50) NOT NULL,
Latitudine VARCHAR(50) NOT NULL,
Indirizzo VARCHAR(255),
Ricarica BOOLEAN,
Citta VARCHAR(50),
PRIMARY KEY (Longitudine, Latitudine)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE VEICOLO(
Targa VARCHAR(10) NOT NULL PRIMARY KEY,
Modello VARCHAR(30),
Marca VARCHAR(30),
Capienza INT,
Descrizione VARCHAR(255),
TariffaFestiva INT,
TariffaFeriale INT,
IDSocieta INT,
Latitudine VARCHAR(50),
Longitudine VARCHAR(50),
 FOREIGN KEY (Longitudine, Latitudine) REFERENCES AREA_SOSTA(Longitudine, Latitudine),
 FOREIGN KEY (IDSocieta) REFERENCES SOCIETA(IDSocieta)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;


CREATE TABLE SEGNALAZIONE(
IDSegnalazione INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
DataSegnalazione DATE,
Titolo VARCHAR (50),
Testo VARCHAR (200),
Targa VARCHAR(10),
IndirizzoEmail VARCHAR(50),
IDSocieta INT,
FOREIGN KEY (Targa) REFERENCES VEICOLO(Targa),
FOREIGN KEY (IndirizzoEmail) REFERENCES UTENTE(IndirizzoEmail),
FOREIGN KEY (IDSocieta) REFERENCES SOCIETA(IDSocieta)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE BROCHURE(
IDBrochure INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
Titolo VARCHAR (30),
Testo VARCHAR (200),
IDSocieta INT,
FOREIGN KEY (IDSocieta) REFERENCES SOCIETA(IDSocieta)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE PRENOTAZIONE(
CodicePrenotazione INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
DataInizio DATE,
OraInizio TIME,
DataFine DATE,
OraFine TIME,
Note VARCHAR(255), 
SpeseCondivise INT,
IndirizzoEmail VARCHAR(50),
Targa VARCHAR(10),
LatitudineArrivo VARCHAR(50),
LongitudineArrivo VARCHAR(50),
LatitudinePartenza VARCHAR(50),
LongitudinePartenza VARCHAR(50),


FOREIGN KEY (LongitudineArrivo, LatitudineArrivo) REFERENCES AREA_SOSTA(Longitudine, Latitudine),
FOREIGN KEY (LongitudinePartenza, LatitudinePartenza) REFERENCES AREA_SOSTA(Longitudine, Latitudine),
FOREIGN KEY (Targa) REFERENCES VEICOLO(Targa),
FOREIGN KEY (IndirizzoEmail) REFERENCES UTENTE(IndirizzoEmail)

)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;


CREATE TABLE TRAGITTO(
IDTragitto INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
NumeroKm INT,
Tipologia CHAR(3)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE PRENOTAZIONE_TRAGITTO (
CodicePrenotazione INT AUTO_INCREMENT NOT NULL PRIMARY KEY,
IDTragitto INT,

FOREIGN KEY (IDTragitto) REFERENCES TRAGITTO(IDTragitto),
FOREIGN KEY (CodicePrenotazione) REFERENCES PRENOTAZIONE(CodicePrenotazione)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE TAPPA(
Latitudine VARCHAR(50),
Longitudine VARCHAR(50),
Citta VARCHAR(30) ,
Via VARCHAR(50),
Stato CHAR(1),
PRIMARY KEY(Latitudine, Longitudine)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;

CREATE TABLE TAPPA_TRAGITTO(
IDTragitto INT AUTO_INCREMENT,
Latitudine VARCHAR(50),
Longitudine VARCHAR(50),
Orario TIME,
Ordine VARCHAR(30),
Stato VARCHAR(1),
PRIMARY KEY(IDTragitto,Latitudine, Longitudine),
FOREIGN KEY (Latitudine, Longitudine) REFERENCES TAPPA(Latitudine,Longitudine),
FOREIGN KEY (IDTragitto) REFERENCES TRAGITTO(IDTragitto)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;


CREATE TABLE CONDIVIDI_TAPPA(
IDTragitto INT AUTO_INCREMENT,
Latitudine VARCHAR(50),
Longitudine VARCHAR(50),
IndirizzoEmail VARCHAR(50) ,
PRIMARY KEY(IDTragitto,Latitudine, Longitudine, IndirizzoEmail),
FOREIGN KEY (IndirizzoEmail) REFERENCES UTENTE(IndirizzoEmail),
FOREIGN KEY (Latitudine, Longitudime, IDTragitto) REFERENCES TAPPA_TRAGITTO(Latitudine, Longitudime, IDTragitto)
)DEFAULT CHARACTER SET UTF8MB4 ENGINE=InnoDB;



-- CREO STORED PROCEDURE

CREATE DEFINER=`root`@`localhost` 
PROCEDURE `INSERT_PARTECIPA`(IN p_IndirizzoEmail VARCHAR(50), IN p_latitudine VARCHAR(50), IN p_longitudine VARCHAR(50), IN p_IDTragitto INT(11))
BEGIN
	INSERT INTO CONDIVIDI_TAPPA(indirizzoEmail, latitudine, longitudine, IDTragitto)
    VALUES (p_IndirizzoEmail, p_latitudine, p_longitudine, p_IDTragitto);
END


CREATE DEFINER=`root`@`localhost` 
PROCEDURE `INSERT_PRENOTAZIONE`(IN p_IndirizzoEmail VARCHAR(50), IN p_DataInizio DATE, IN p_OraInizio TIME, IN p_DataFine DATE, IN p_OraFine TIME, IN p_Targa VARCHAR(10), IN p_Note varchar(255), IN p_LatitudineArrivo VARCHAR(50), IN p_LongitudineArrivo VARCHAR(50), IN p_LatitudinePartenza VARCHAR(50), IN p_LongitudinePartenza VARCHAR(50))
BEGIN
INSERT INTO PRENOTAZIONE(IndirizzoEmail, DataInizio, OraInizio, DataFine, OraFine, Targa, Note, LatitudineArrivo, LongitudineArrivo, LatitudinePartenza, LongitudinePartenza) 
VALUES (p_IndirizzoEmail, p_DataInizio, p_OraInizio, p_DataFine, p_OraFine, p_Targa, p_Note, p_LatitudineArrivo, p_LongitudineArrivo, p_LatitudinePartenza, p_LongitudinePartenza);
END


CREATE DEFINER=`root`@`localhost` 
PROCEDURE `INSERT_PRENOTAZIONE_PREMIUM_AZIENDALE`(IN p_IndirizzoEmail VARCHAR(50), IN p_DataInizio DATE, IN p_OraInizio TIME, IN p_DataFine DATE, IN p_OraFine TIME, IN p_Targa VARCHAR(10), IN p_Note VARCHAR(255), IN p_SpeseCondivise INT, IN p_LatitudineArrivo VARCHAR(50), IN p_LongitudineArrivo VARCHAR(50), IN p_LatitudinePartenza VARCHAR(50), IN p_LongitudinePartenza VARCHAR(50))
BEGIN
INSERT INTO PRENOTAZIONE(IndirizzoEmail, DataInizio,OraInizio, DataFine, OraFine, Targa, Note, SpeseCondivise, LatitudineArrivo, LongitudineArrivo, LatitudinePartenza, LongitudinePartenza) 
VALUES (p_IndirizzoEmail, p_DataInizio, p_OraInizio, p_DataFine, p_OraFine, p_Targa, p_Note, p_SpeseCondivise, p_LatitudineArrivo, p_LongitudineArrivo, p_LatitudinePartenza, p_LongitudinePartenza);
END


CREATE DEFINER=`root`@`localhost` 
PROCEDURE `INSERT_SEGNALAZIONE`(IN p_IndirizzoEmail VARCHAR(50), IN p_Data DATE, IN p_Titolo VARCHAR(50), IN p_Testo VARCHAR(200), IN p_Targa VARCHAR(10))
BEGIN
INSERT INTO SEGNALAZIONE(IndirizzoEmail, DataSegnalazione, Titolo, Testo, Targa) 
VALUES (p_IndirizzoEmail, p_Data, p_Titolo, p_Testo, p_Targa);
END

CREATE DEFINER=`root`@`localhost` 
PROCEDURE `INSERT_TAPPA`(IN p_IDTragitto INT(11), IN p_Orario TIME, IN p_Ordine INT, IN p_Citta VARCHAR(30), IN p_Via VARCHAR(50), IN p_latitudine VARCHAR(50), IN p_longitudine VARCHAR(50))
BEGIN
DECLARE num INT;
	INSERT INTO TAPPA(Citta, Via, latitudine, longitudine) 
    VALUES (p_Citta, p_Via, p_latitudine, p_longitudine);
    INSERT INTO TAPPA_TRAGITTO(Orario, Ordine, IDTragitto, latitudine, longitudine) 
    VALUES (p_Orario, p_Ordine, p_IDTragitto, p_latitudine, p_longitudine);
END


CREATE DEFINER=`root`@`localhost` 
PROCEDURE `INSERT_TRAGITTO`(IN p_tipologia CHAR(3), IN p_num_km INT, IN p_codiceprenotazione INT)
BEGIN
DECLARE num INT;
	INSERT INTO TRAGITTO(tipologia, numerokm)
    VALUES (p_tipologia, p_num_km);
    SELECT LAST_INSERT_ID() into num;
    INSERT INTO PRENOTAZIONE_TRAGITTO(codiceprenotazione, idtragitto) 
    VALUES (p_codiceprenotazione, num);
END


CREATE DEFINER=`root`@`localhost` 
PROCEDURE `INSERT_UTENTE`(IN p_IndirizzoEmail VARCHAR(50), IN p_Pwd VARCHAR(32), IN p_Nome VARCHAR(50), IN p_Cognome VARCHAR(50), IN p_codazienda INT(11), IN p_tipoutente CHAR(1), IN p_datanascita DATE, IN p_luogonascita VARCHAR(50) )
BEGIN
INSERT INTO UTENTE(IndirizzoEmail, Pwd, Nome, Cognome, codazienda, tipoutente, datanascita, luogonascita)
 VALUES (p_IndirizzoEmail, p_Pwd, p_Nome, p_Cognome, p_codazienda, p_tipoutente, p_datanascita, p_luogonascita);
END


CREATE DEFINER=`root`@`localhost` PROCEDURE `INSERT_VOTO`(IN p_IndirizzoEmail VARCHAR(50), IN p_DataVoto DATE, IN p_Voto INT(11), IN p_Testo VARCHAR(500))
BEGIN
INSERT INTO FEEDBACK(IndirizzoEmail, DataVoto, Voto, Testo) 
VALUES (p_IndirizzoEmail, p_DataVoto, p_Voto, p_Testo);
END



--             ----------- CREO TRIGGER  ------------


-- CHIUSURA VEICOLO CAPIENZA

CREATE DEFINER=`root`@`localhost` TRIGGER chiuditappa
    AFTER INSERT ON condividi_tappa
    FOR EACH ROW
    BEGIN
    DECLARE cap INT DEFAULT 0;
    DECLARE num INT DEFAULT 0;
        

    
    
    
    
    SET cap := (SELECT capienza FROM veicolo 
    INNER JOIN prenotazione ON veicolo.targa = prenotazione.targa 
    INNER JOIN prenotazione_tragitto ON prenotazione.codiceprenotazione = prenotazione_tragitto.codiceprenotazione 
    WHERE prenotazione.indirizzoemail !=  new.indirizzoemail AND prenotazione_tragitto.idtragitto = NEW.idtragitto LIMIT 1);
    

    SET num := (select count(*) from condividi_tappa where idtragitto=NEW.idtragitto);
    SET num:=num+1;
    if (num=cap) THEN
		UPDATE tappa_tragitto
		SET tappa_tragitto.stato = 'C'
		WHERE Stato = 'A' AND  idtragitto= NEW.idtragitto;
	END IF;


    
		
    
END




-- DA SEMPLICE A PREMIUM

CREATE DEFINER=`root`@`localhost` TRIGGER semptoprem
    AFTER INSERT ON prenotazione
    FOR EACH ROW
    UPDATE utente
    SET TipoUtente = 'P'
    WHERE TipoUtente = 'S' AND IndirizzoEmail IN(
    SELECT IndirizzoEmail
	FROM prenotazione
    GROUP BY IndirizzoEmail
    HAVING COUNT(*) >= 3)
